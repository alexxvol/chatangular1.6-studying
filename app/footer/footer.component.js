function footerController(mainSrv) {
    var $ctrl = this;
    $ctrl.labelFooter = "its FOOTER";
    $ctrl.name = "Alexx"

    //from header to footer
    $ctrl.testfooter = $ctrl.testfooterhead;

    // testing get data from service
    $ctrl.sayNameFooter = mainSrv.sayName;
    //this function = function from service

    mainSrv.name = $ctrl.name;
}

footerApp.component('footerComponent', {
    templateUrl: 'footer/footer.html',
    controller: footerController,
    bindings: {

    }



})