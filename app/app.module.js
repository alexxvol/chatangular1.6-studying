'use strict';



function myApp(mainSrv) {
    var $ctrl = this;
    $ctrl.name = "BOB";

}


var app = angular.module('mainApp', ['headerModule', 'footerModule', 'containerModule', 'ui.router']);


app.component('app', {
    templateUrl: 'main.html',
    controller: myApp
})

app.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('chat', {
            url: '/',
            templateUrl: 'main.html'
        })
        .state('about', {
            url: '/about',
            templateUrl: 'staff/about.html'
        })
        .state('notes', {
            url: '/notes',
            templateUrl: 'staff/notes.html'
        })
        .state('links', {
            url: '/links',
            templateUrl: 'staff/links.html'
        })

})