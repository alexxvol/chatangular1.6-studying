function dialogsController($http, $interval, httpService) {
    var $ctrl = this;
    $ctrl.$onInit = function() {
        // $ctrl.testName = $ctrl.testnamer || "NA";
        // $ctrl.username = $ctrl.usernameb || 'NA';
    }

    // $ctrl.userDialogs = ['blablabla', 'tralalala'];
    // $ctrl.addDialog = function(dialog) {
    //     if (dialog) {
    //         $ctrl.userDialogs.push(dialog);
    //     } else {
    //         console.log('empty message!')
    //     }
    // };


    // $ctrl.getMessages = function() {
    //         $http.get('https://serverforchat-alexxvol.c9users.io:8081/messages').then(function(response) {
    //             $ctrl.posts = response.data;
    //             console.log($ctrl.posts)
    //         });
    //     }
    // $ctrl.getMessages();

    $ctrl.getMessages = function() {
        httpService.getPosts().then(function(response) {
            $ctrl.posts = response.data;
            console.log($ctrl.posts)
        });
    }


    $interval($ctrl.getMessages, 1000);

    $ctrl.formatMess = function(key, post) {
        var _key = document.getElementById(key);
        _key.innerHTML = '<b> ' + post.user_id + '</b> : ' + '<span>' + post.message + '</span>';
    }
}

containerApp.component('dialogsComponent', {
    bindings: {
        testnamer: '@',
        usernameb: '<'
    },
    templateUrl: 'container/dialogs/dialogs.html',
    controller: dialogsController
})