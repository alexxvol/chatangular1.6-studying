function containerController(mainSrv) {
    var $ctrl = this;
    $ctrl.users = "user list";

    //testing parent to child
    $ctrl.testdataparent2 = "text from container!!";

}

containerApp.component('containerComponent', {
    templateUrl: 'container/container.html',
    controller: containerController,
    bindings: {}

})