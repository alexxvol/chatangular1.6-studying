function userlistController($http, httpService, mainSrv) {
    var $ctrl = this;
    $ctrl.myname = "";
    $ctrl.server = 'https://serverforchat-alexxvol.c9users.io:8081';

    //testing parent into child
    $ctrl.testdata = $ctrl.testdatar;


    //get userList from server
    $ctrl.getUsers = function() {
        httpService.getUsers().then(function(response) {
            $ctrl.userlist = response.data;
        });
    }
    $ctrl.getUsers();

    //register new user and send to server
    $ctrl.registerUser = function() {
        if ($ctrl.myname) {
            $http.post($ctrl.server + "/users/register", {
                "username": $ctrl.myname
            }).then(function(response) {
                $ctrl.getUsers();
                console.log(response);
            });
            $ctrl.myname = "";
        } else {
            alert("Зарегестрируйтесь!")
        }
    }
}
containerApp.component('userlistComponent', {
    templateUrl: 'container/userlist/userlist.html',
    controller: userlistController,
    bindings: {
        testdatar: '<'
    }

})

//    this.add = function() {
//         if (this.objectPhones.inpName && this.objectPhones.inpSecondName && this.objectPhones.inpPhone && this.objectPhones.inpDate) {
//             $http.post(this.server, {
//                 firstName: this.objectPhones.inpName,
//                 secondName: this.objectPhones.inpSecondName,
//                 phone: this.objectPhones.inpPhone,
//                 createdAt: this.objectPhones.inpDate
//             }).then(function(response) {
//                 console.log(response);
//             });
//         } else {
//             console.log("Заполните поля")
//         }
//     };