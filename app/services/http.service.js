app.service('httpService', function($http) {


    // GET posts from server
    this.getPosts = function() {
        return $http.get('https://serverforchat-alexxvol.c9users.io:8081/messages')
    }

    // GET users from server
    this.getUsers = function() {
        return $http.get('https://serverforchat-alexxvol.c9users.io:8081/users')
    }


})