function headerController() {
    var $ctrl = this;
    $ctrl.label = "Angular 1.6(study CHAT)";
    $ctrl.test = "forTestHeader";
}

headerApp.component('headerComponent', {
    templateUrl: 'header/header.html',
    controller: headerController,
    // controllerAs: "vm"
    bindings: {

    }
})